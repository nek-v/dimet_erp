from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#########################
#  Crm Lead/Opportunity #
#########################
class DMLead(models.Model):
    _inherit = "crm.lead"

    # Fields
    name = fields.Char(translate=True)
    sale_orders = fields.One2many(string="Quotations/Sales Orders", comodel_name='sale.order',
                                  inverse_name='opportunity')
    sale_order_count = fields.Integer(string="Quotations/Sales Orders", compute='_count_sale_orders')
    bp_nps = fields.One2many(string="Product Design Requests", comodel_name='dm.bp.np',
                             inverse_name='opportunity_id')
    bp_np_count = fields.Integer(string="Product Design Requests", compute='_count_bp_nps')
    lead_manager = fields.Many2one(string="Lead manager", comodel_name='res.users')
    user_id = fields.Many2one(required=False)
    country_id = fields.Many2one(required=False)
    planned_revenue = fields.Float(string="Planned revenue", compute='get_planned_revenue', store=True)
    lead_potential = fields.Float(string="Lead potential", compute='get_lead_potential', store=True)

# -- Compute planned revenue
    @api.multi
    @api.depends('probability', 'planned_revenue', 'stage_id')
    def get_lead_potential(self):
        for rec in self:
            rec.lead_potential = rec.planned_revenue * rec.probability / 100

    # -- Compute planned revenue
    @api.multi
    @api.depends('sale_orders.state', 'sale_orders.amount_total')
    def get_planned_revenue(self):
        for rec in self:
            sale_orders = self.env['sale.order'].sudo().search(['&', ('opportunity', '=', rec.id), ('state', 'in', ['draft', 'sent'])])
            planned_revenue = 0
            for order in sale_orders:
                planned_revenue += order.amount_total
            rec.planned_revenue = planned_revenue

# -- Create
    @api.model
    def create(self, vals):
        # Add lead manager as follower

        if 'message_follower_ids' in vals:
            if 'lead_manager' in vals:
                if vals['lead_manager']:
                    if 'user_id' in vals:
                        if vals['lead_manager'] != vals['user_id']:
                            partner_id = self.env['res.users'].browse(vals['lead_manager']).partner_id.id
                            if vals['message_follower_ids']:
                                vals['message_follower_ids'].append([4, partner_id])
                            else:
                                vals['message_follower_ids'] = [[4, partner_id]]
        return super(DMLead, self).create(vals)


# -- Write
    @api.multi
    def write(self, vals):

        # Subscribe person task assigned to if not already assigned
        if 'lead_manager' in vals and vals['lead_manager']:
            lead_manager = self.env['res.users'].sudo().browse([vals['lead_manager']]).partner_id
            if lead_manager:
                lead_manager_id = lead_manager.id
                for rec in self:
                    if lead_manager_id not in rec.message_follower_ids.ids:
                        rec.message_subscribe(partner_ids=[lead_manager_id])

        return super(DMLead, self).write(vals)


# -- Partner changed
    @api.multi
    def on_change_partner_id(self, partner_id):
        values = {}
        if partner_id:
            partner = self.env['res.partner'].browse([partner_id])
            partner_name = (partner.parent_id and partner.parent_id.name) or (partner.is_company and partner.name) or False
            if partner.lead_manager:
                lead_manager = partner.lead_manager.id
            else:
                is_lead_manager = self.env['res.users'].has_group('dimet_erp.dm_lead_manager')
                lead_manager = self._uid if is_lead_manager else False

            values = {
                'user_id': partner.user_id.id if partner.user_id else False,
                'section_id': partner.section_id.id if partner.section_id else False,
                'lead_manager': lead_manager,
                'partner_name': partner_name,
                'contact_name': (not partner.is_company and partner.name) or False,
                'title': partner.title and partner.title.id or False,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id and partner.state_id.id or False,
                'country_id': partner.country_id and partner.country_id.id or False,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'fax': partner.fax,
                'zip': partner.zip,
                'function': partner.function,
            }
        return {'value': values}

# -- Set lead manager (old)
    @api.multi
    def set_lead_manager(self):
        for rec in self:
            rec.user_id = rec.partner_id.user_id.id if rec.partner_id.user_id else False
            rec.section_id = rec.partner_id.section_id.id if rec.partner_id.section_id else False
            if rec.partner_id.lead_manager:
                rec.lead_manager = rec.partner_id.lead_manager.id
            else:
                is_lead_manager = self.env['res.users'].has_group('dimet_erp.dm_lead_manager')
                rec.lead_manager = self._uid if is_lead_manager else False


# -- Count Product Design Request
    @api.multi
    def _count_bp_nps(self):
        for rec in self:
            rec.bp_np_count = self.env['dm.bp.np'].search_count([('opportunity_id', '=', rec.id)])

# -- Open Product Design Requests
    @api.multi
    def open_bp_nps(self):
        self.ensure_one()
        current_id = self.id
        context = {
            'default_opportunity_id': current_id
        }

        # Compose view header
        header = _("Related to ") + self.name

        return {
            'name': header,
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'dm.bp.np',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': [('opportunity_id', '=', current_id)]
        }

# -- New Product Design Request
    @api.multi
    def button_bp_np(self):
        self.ensure_one()
        current_id = self.id
        context = {
            'default_opportunity_id': current_id,
            'default_name': self.name
        }

        # Compose view header
        header = _("Related to ") + self.name

        return {
            'name': header,
            "views": [[False, "form"]],
            'res_model': 'dm.bp.np',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': [('opportunity_id', '=', current_id)]
        }

# -- Count Sale Orders
    @api.multi
    def _count_sale_orders(self):
        for rec in self:
            rec.sale_order_count = self.env['sale.order'].search_count([('opportunity', '=', rec.id)])

# -- Open Sale Orders
    @api.multi
    def open_sale_orders(self):
        self.ensure_one()
        current_id = self.id
        context = {
            'default_opportunity': current_id,
            'default_partner_id': self.partner_id.id

        }

        # Compose view header
        header = _("Related to ") + self.name

        return {
            'name': header,
            "views": [[False, "tree"], [False, "form"]],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': [('opportunity', '=', current_id)]
        }

# -- New Quotation
    @api.multi
    def button_quotation(self):
        self.ensure_one()
        current_id = self.id
        context = {
            'default_opportunity': current_id,
            'default_partner_id': self.partner_id.id

        }

        # Compose view header
        header = _("Related to ") + self.name

        return {
            'name': header,
            "views": [[False, "form"]],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': [('opportunity', '=', current_id)]
        }

# -- Mark Lost
    @api.multi
    def button_lost(self):
        self.ensure_one()
        opportunity_id = self.id

        # Check if loss report already exists
        loss_report = self.env['dm.opportunity.loss'].search([('opportunity_id', '=', opportunity_id)])

        # Open existing report if exist
        if loss_report:
            return {
                'views': [[False, "form"]],
                'res_model': 'dm.opportunity.loss',
                'type': 'ir.actions.act_window',
                'target': 'current',
                'res_id': loss_report.id,
            }

        # Else open new report wizard
        context = {
            'default_opportunity_id': opportunity_id,
            'default_partner_id': self.partner_id.id,
            'default_salesman': self.user_id.id,
            'default_lead_manager': self.lead_manager.id,
        }

        return {
            'views': [[False, "form"]],
            'res_model': 'dm.opportunity.loss.wiz',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
        }


#################################
#  Opportunity2Quotation Wizard #
#################################
class DMCrmMakeSale(models.Model):
    _inherit = "crm.make.sale"

# - Override makeOrder function
    @api.model
    def makeOrder(self):
        self._context['default_opportunity'] = self._context.get('active_id')
        value = super(DMCrmMakeSale, self).makeOrder()
        return value


#####################
#  Opportunity loss #
#####################
class DMOpportunityLoss(models.Model):
    _name = "dm.opportunity.loss"

# -- Fields
    opportunity_id = fields.Many2one(string="Opportunity", comodel_name='crm.lead')
    sales_order_id = fields.Many2one(string="Quotation/Sales Order", comodel_name='sale.order')
    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner')
    partner_name = fields.Char(string="Partner", related='partner_id.name')
    name = fields.Char(string="Name", compute='compose_name')
    salesman = fields.Many2one(string="Salesperson", comodel_name='res.users')
    lead_manager = fields.Many2one(string="Lead manager", comodel_name='res.users')
    # Fields
    reason = fields.Selection([
        ('0', 'Price'),
        ('1', 'Payment terms'),
        ('2', 'Delivery terms'),
        ('3', 'Technical incompleteness'),
        ('4', 'Warranty terms'),
        ('5', 'Brand weakness'),
        ('6', 'We canceled'),
        ('7', 'Customer canceled'),
        ('8', 'Offer deadline missed'),
        ('9', 'Validity terms'),
        ('x', 'Other'),
    ], string="Key loss reason", required=True)
    note = fields.Text(string="Description", required=True,
                       help="Describe why the deal was lost")
    winner_id = fields.Many2one(string="Winner", comodel_name='res.partner',
                                help="Winner, if known", auto_join=True)
    winner_name = fields.Char(string="Winner", related='winner_id.name')
    winner_price = fields.Float(string="Winning price, total",
                                help="Winning price including taxes, delivery etc")

    # SQL constraints
    _sql_constraints = [('opp_loss_unique',
                         'UNIQUE (opportunity_id,sales_order_id)',
                         _('Report already exists, please edit existing report!'))]

# -- Opportunity change
    @api.multi
    @api.onchange('opportunity_id')
    def opp_change(self):
        self.ensure_one()
        self.update({'partner_id': self.opportunity_id.partner_id.id,
                     'salesman': self.opportunity_id.user_id.id,
                     'lead_manager': self.opportunity_id.lead_manager.id,
                     })

# -- Sales Order change
    @api.multi
    @api.onchange('sales_order_id')
    def so_change(self):
        self.ensure_one()
        self.update({'partner_id': self.sales_order_id.partner_id.id,
                     'salesman': self.sales_order_id.user_id.id,
                     'lead_manager': self.sales_order_id.lead_manager.id
                     })

# -- Search name
    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        new_args = [('opportunity_id.name', 'ilike', name)]
        #return super(DMOpportunityLoss, self).name_search(name=name, agrs=new_args, limit=limit)
        return self.search([('opportunity_id.name', 'ilike', name)])

# -- Compose name
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.sales_order_id.name if rec.sales_order_id else rec.opportunity_id.name


############################
#  Opportunity loss wizard #
############################
class DMOpportunityLossWiz(models.TransientModel):
    _name = "dm.opportunity.loss.wiz"

# -- Fields
    opportunity_id = fields.Many2one(string="Opportunity", comodel_name='crm.lead')
    sales_order_id = fields.Many2one(string="Quotation/Sales Order", comodel_name='sale.order')
    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner')
    name = fields.Char(string="Name", compute='compose_name')
    salesman = fields.Many2one(string="Salesperson", comodel_name='res.users')
    lead_manager = fields.Many2one(string="Lead manager", comodel_name='res.users')
    # Fields
    reason = fields.Selection([
        ('0', 'Price'),
        ('1', 'Payment terms'),
        ('2', 'Delivery terms'),
        ('3', 'Technical incompleteness'),
        ('4', 'Warranty terms'),
        ('5', 'Brand weakness'),
        ('6', 'We canceled'),
        ('7', 'Customer canceled'),
        ('8', 'Offer deadline missed'),
        ('9', 'Validity terms'),
        ('x', 'Other'),
    ], string="Key loss reason", required=True)
    note = fields.Text(string="Description", required=True,
                       help="Describe why the deal was lost")
    winner_id = fields.Many2one(string="Winner", comodel_name='res.partner',
                                help="Winner, if known", auto_join=True)
    winner_name = fields.Char(string="Winner", related='winner_id.name')
    winner_price = fields.Float(string="Winning price, total",
                                help="Winning price including taxes, delivery etc")
    cancel_orders = fields.Boolean(string="Cancel related Quotations", default=True,
                                   help="Any confirmed orders must be canceled manual!")


# -- Compose name
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.sales_order_id.name if rec.sales_order_id else rec.opportunity_id.name

# -- Save loss report
    @api.multi
    def button_save(self):
        self.ensure_one()

        # Record loss reason
        vals = {
            'reason': self.reason,
            'note': self.note,
        }

        if self.winner_id:
            vals['winner_id'] = self.winner_id.id

        if self.winner_price > 0:
            vals['winner_price'] = self.winner_price

        # If Opportunity
        if self.opportunity_id:
            opportunity_id = self.opportunity_id.id
            vals['opportunity_id'] = opportunity_id
            if self.opportunity_id.partner_id:
                vals['partner_id'] = self.opportunity_id.partner_id.id
            if self.opportunity_id.user_id:
                vals['salesman'] = self.opportunity_id.user_id.id
            if self.opportunity_id.lead_manager:
                vals['lead_manager'] = self.opportunity_id.lead_manager.id

            # Mark opportunity as lost
            opp_lost = self.env['crm.lead'].search([('id', '=', opportunity_id)]).case_mark_lost()

            # Cancel related quotations
            if opp_lost and self.cancel_orders:
                orders = self.env['sale.order'].search(['&', ('opportunity', '=', opportunity_id),
                                                        ('state', 'in', ['draft', 'sent'])])
                for order in orders:
                    order.action_cancel()

        # If Sales Order
        elif self.sales_order_id:
            sales_order = self.sales_order_id
            sales_order_id = sales_order.id
            vals['sales_order_id'] = sales_order_id
            if sales_order.partner_id:
                vals['partner_id'] = sales_order.partner_id.id
            if sales_order.user_id:
                vals['salesman'] = sales_order.user_id.id
            if sales_order.lead_manager:
                vals['lead_manager'] = sales_order.lead_manager.id

            # If Sales Order linked to Opportunity mark related Opportunity as lost
            if sales_order.opportunity:
                opportunity_id = sales_order.opportunity.id

                # Check if any other NOT canceled Sales Orders exist for this Opportunity
                # If not exist mark related Opportunity as lost
                other_orders_count = self.env['sale.order'].sudo().search_count(['&', ('id', '!=', sales_order_id),
                                                                                 ('state', '!=', 'cancel')])
                if other_orders_count == 0:
                    self.env['crm.lead'].search([('id', '=', opportunity_id)]).case_mark_lost()

        else:
            # Exit doing nothing
            return

        # Create Loss Report
        self.env['dm.opportunity.loss'].create(vals)
        return



