from openerp import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
import openerp.addons.decimal_precision as dp
import re

import logging
# Logger for debug
_logger = logging.getLogger(__name__)

# Maximum amount to put on balance for auto validation
# If difference between amount_unreconciled and amount_voucher is bigger mark voucher as "Attention required"
VOUCHER_MAX_OVERPAY = 100


##########################
# Account voucher wizard #
##########################
class DMAccountVoucherWiz(models.TransientModel):
    _name = "dm.account.voucher.wiz"

# -- Cancel vouchers
    @api.multi
    def button_voucher_cancel(self):
        self.env['account.voucher'].browse(self._context.get('active_ids')).cancel_voucher()

# -- Set vouchers to draft
    @api.multi
    def button_voucher_set_draft(self):
        # self.env['account.voucher'].browse(self._context.get('active_ids')).action_cancel_draft()
        vouchers = self.env['account.voucher'].search(['&', ('state', '=', 'cancel'), ('id', 'in', (self._context.get('active_ids')))])
        vouchers.action_cancel_draft()
        # Remove existing credit and debit lines
        for voucher in vouchers:
            voucher.action_cancel_draft()
            res = {}
            del_cr = []
            if voucher.line_cr_ids.ids:
                for line_cr_id in voucher.line_cr_ids.ids:
                    del_cr.append([2, line_cr_id, False])
                    res.update({"line_cr_ids": del_cr,})

            del_dr = []
            if voucher.line_dr_ids.ids:
                for line_dr_id in voucher.line_dr_ids.ids:
                    del_cr.append([2, line_dr_id, False])
                    res.update({"line_dr_ids": del_dr,})

            if len(res) > 0:
                voucher.write(res)

# -- Find referenced Sales Order and re-validate vouchers
    @api.multi
    def button_voucher_scan_so(self):
        # self.env['account.voucher'].browse(self._context.get('active_ids')).action_cancel_draft()
        vouchers = self.env['account.voucher'].search(['&', ('state', '=', 'draft'), ('id', 'in', self._context.get('active_ids'))], order='id desc')
        # Scan vouchers for SO reference
        vouchers.sudo().find_linked_order()

# -- Find vouchers linked to several Sales Orders
    @api.multi
    def button_voucher_attention_so(self):
        self.env['account.voucher'].browse(self._context.get('active_ids')).sudo().find_attention_required()


###################
# Account voucher #
###################
class DMAccountVoucher(models.Model):
    _inherit = "account.voucher"

    inn = fields.Char(string="INN", compute="_get_inn", inverse="_set_inn")
    line_dr_ids = fields.One2many(readonly=False)
    line_cr_ids = fields.One2many(readonly=False)
    amount_confirm = fields.Float(string="Enter amount again to validate", compute="_dummy", inverse="_dummy",
                                  digits_compute=dp.get_precision('Account'))
    pre_check = fields.Boolean(string="Amount is correct", compute="_pre_check")
    auto_validation = fields.Selection([
        ('v', 'Validated'),
        ('e', 'Error'),
        ('a', 'Attention required')
    ],  string="Auto Validation", help="Auto validation status")


# -- New "Validate" button
    @api.multi
    def new_validate_button(self):
        self.write({'auto_validation': False})
        self.signal_workflow('proforma_voucher')

# -- Silence onchange function for amount
    @api.v7
    def onchange_amount(self, cr, uid, ids, amount, rate, partner_id, journal_id, currency_id, ttype, date,
                        payment_rate_currency_id, company_id, context=None):
        return

# -- Silence onchange function for journal_id
    @api.v7
    def onchange_journal(self, cr, uid, ids, journal_id, line_ids, tax_id, partner_id, date, amount,
                         ttype, company_id, context=None):
        return

# -- Silence onchange function for partner_id
    @api.v7
    def onchange_partner_id(self, cr, uid, ids, partner_id, journal_id, amount, currency_id, ttype,
                            date, context=None):

        partner_pool = self.pool.get('res.partner')
        journal_pool = self.pool.get('account.journal')
        res = {'account_id': False}
        if not partner_id or not journal_id:
            return res

        journal = journal_pool.browse(cr, uid, journal_id, context=context)
        partner = partner_pool.browse(cr, uid, partner_id, context=context)
        account_id = False
        if journal.type in ('sale','sale_refund'):
            account_id = partner.property_account_receivable.id
        elif journal.type in ('purchase', 'purchase_refund','expense'):
            account_id = partner.property_account_payable.id
        elif ttype in ('sale', 'receipt'):
            account_id = journal.default_debit_account_id.id
        elif ttype in ('purchase', 'payment'):
            account_id = journal.default_credit_account_id.id
        else:
            account_id = journal.default_credit_account_id.id or journal.default_debit_account_id.id

        res['account_id'] = account_id

        rec = self.browse(cr, uid, ids, context=context)
        if rec.line_cr_ids:
            del_cr = []
            for line_cr_id in rec.line_cr_ids.ids:
                del_cr.append([2, line_cr_id, False])
                res.update({"line_cr_ids": del_cr, })

        if rec.line_dr_ids:
            del_dr = []
            for line_dr_id in rec.line_dr_ids.ids:
                del_dr.append([2, line_dr_id, False])
                res.update({"line_dr_ids": del_dr, })

        _logger.info("Onchange res: %s", res)

        return {
            'value': res,
        }

# -- Amount confirmation check
    @api.depends('amount_confirm', 'amount')
    @api.multi
    def _pre_check(self):
        self.ensure_one()
        self.pre_check = True if int(round(self.amount * 100)) == int(round(self.amount_confirm * 100))and self.amount > 0 else False

    # -- Dummy
    @api.multi
    def _dummy(self):
        return

# -- Get INN
    @api.depends('partner_id')
    @api.multi
    def _get_inn(self):
        self.ensure_one()
        if self.partner_id:
            self.inn = self.partner_id.inn_d

# -- Set INN
    @api.onchange('inn')
    @api.multi
    def _set_inn(self):
        self.ensure_one()
        # TODO set minimum len to 4
        if self.inn and len(self.inn) > 3:
            return {'domain': {'partner_id': [('inn_d', 'like', self.inn)]}}
        else:
            return {'domain': {'partner_id': []}}


# -- Parse string and search related document by name
    """
    @model - model to scan (sale.order, purchase.order)
    @partner_id - id of partner invoiced
    @company_id - id of the company
    """
    @api.multi
    def parse_document(self, model, partner_id, company_id, arg):
        # Find sequence for our company first
        sequence_list = self.env['ir.sequence'].sudo().search([
            '&',
            ('company_id', '=', company_id),
            ('code', '=', model),
        ])

        # If our company_id is not found, get company_id is NULL
        if not sequence_list:
            sequence_list = self.env['ir.sequence'].sudo().search([('code', '=', model)])

        # Loop all sequence
        for seq in sequence_list:
            regex = seq.prefix + '\s*\d+'
            compiled = re.compile(regex, re.I)
            list_order_names = re.findall(compiled, arg)
            list_order_names = [name.replace(' ', '') for name in list_order_names]

            order_list_names_confirmed = []
            order_list_ids_confirmed = []

            for name in list_order_names:
                order_list = self.env[model].sudo().search([
                    '&', '&',
                    ('partner_invoice_id', '=', partner_id),
                    ('company_id', '=', company_id),
                    ('name', 'ilike', name),
                ])
                if order_list:
                    # list_order_names.remove(name)
                    order_list_ids_confirmed += order_list.ids
                    order_list_names_confirmed.append(name)
            list_order_names = [i for i in list_order_names if i not in order_list_names_confirmed]

            return {"confirmed_ids": order_list_ids_confirmed,
                    "possible_names": list_order_names}


# Find related Sales Orders and change their state and create invoices if necessary
# Button find orders
    @api.multi
    def find_linked_order_button(self):
        ctx = self._context.copy()
        ctx.update({'notify_so': 'auto'})
        self.with_context(ctx).find_linked_order()

# - Find linked Sales Order
    """
    Confirms order (sales), creates and validates invoices.
    If multiple Sales Order detected in single voucher or any problem with sales order occurs
    send notification to related Sales Orders and voucher, mark voucher as "Attention required" 
    """
    @api.multi
    def find_linked_order(self):
        # Array for not validated orders
        not_validated = []
        for rec in self:

            # TODO Only incoming vouchers are proceeded!
            if not rec.type == 'receipt':
                continue

            # Only vouchers in 'draft' state are proceeded!
            if not rec.state == 'draft':
                continue

            # Skip if payment information is missing
            if not rec.name or not rec.reference:
                continue

            # Proceed Sales Order only for the moment. Capitalize before detection
            order_type = 'sale.order'

            res = self.parse_document(order_type, rec.partner_id.id, rec.company_id.id, rec.name.upper())
            if not res:
                # Mark voucher auto validation error, send notification to partner
                not_validated.append(rec.id)
                if rec.partner_id:
                    message_text = _("Incoming payment number ") + rec.reference + _(
                        " requires manual validation by accountant! "
                        "Payment reference: ") + rec.name
                    # Notify
                    rec.partner_id.sudo().message_post(type="notification", subtype="mt_comment",
                                                       body=message_text)
                continue
            else:
                unchecked_ids = res['confirmed_ids']
                if not unchecked_ids or len(unchecked_ids) < 1:
                    # Mark voucher auto validation error, send notification to partner
                    not_validated.append(rec.id)
                    if rec.partner_id:
                        message_text = _("Incoming payment number ") + rec.reference + _(
                            " requires manual validation by accountant! "
                            "Payment reference: ") + rec.name
                        # Notify
                        rec.partner_id.sudo().message_post(type="notification", subtype="mt_comment",
                                                           body=message_text)
                    continue
                else:
                    # Remove duplicated ids if any
                    confirmed_ids = [unchecked_ids[0]]
                    for unchecked_id in unchecked_ids:
                        if unchecked_id not in confirmed_ids:
                            confirmed_ids.append(unchecked_id)

                    # Store voucher amount to var
                    amount = rec.amount

                    # Mark voucher as "Validated" if voucher has only one Sales order else mark as "Attention required"
                    auto_validation = 'a' if len(confirmed_ids) > 1 else "v"
                    confirmed_orders = self.env['sale.order'].sudo().browse(confirmed_ids)

                    # Loop trough orders
                    for order in confirmed_orders:
                        # _logger.info("Sales Order %s detected in state %s", order.name, order.state)

                        # Confirm Quotation if not confirmed yet
                        if order.state in ['draft', 'sent']:
                            order.sudo().action_button_confirm()
                            # _logger.info("Quotation %s confirmed", order.name)

                        # Create invoice if not created yet
                        # Remove context to avoid errors!!!
                        if not order.sudo().invoice_exists:
                            order.sudo().with_context({}).action_invoice_create()
                            # _logger.info("Invoice for Sales Order %s created", order.name)

                        # Validate all related non-validated invoices of Sales Order
                        invoices = self.env['account.invoice'].sudo().browse(order.invoice_ids.ids)
                        for inv in invoices:
                            if inv.state in ['draft', 'proforma', 'proforma2']:
                                inv.sudo().signal_workflow('invoice_open')
                                #_logger.info("Invoice %s validated", inv.name)

                        # If there is more than one invoice or no invoices for Order continue because we cannot compose
                        #  credit line correctly
                        # Mark Voucher as "Attention required" and continue
                        if not invoices or len(invoices) > 1:
                            auto_validation = 'a'

                        # If Attention is required do not proceed with credit and debit lines
                        if auto_validation == 'a':
                            continue

                        # Create Credit voucher lines
                        # TODO more than one credit line can exist because of Payment Terms
                        move_line = self.env['account.move.line'].sudo().search(
                            ['&', '&', ('move_id', '=', invoices[0].move_id.id), ('account_id.type', '=', 'receivable'),
                             ('reconcile_id', '=', False)], order='id desc')
                        move_line_id = move_line[0].id if move_line else False

                        # Mark "Attention required" if cannot compose move line
                        if not move_line_id or len(move_line) > 1:
                            auto_validation = 'a'
                            continue

                        # Remove existing credit and debit lines
                        if rec.line_cr_ids:
                            del_cr = []
                            for line_cr_id in rec.line_cr_ids.ids:
                                del_cr.append([2, line_cr_id, False])
                                rec.write({"line_cr_ids": del_cr,})

                        if rec.line_dr_ids:
                            del_dr = []
                            for line_dr_id in rec.line_dr_ids.ids:
                                del_dr.append([2, line_dr_id, False])
                                rec.write({"line_dr_ids": del_dr, })

                        # Compose CR line. order_ids ONLY for notification in account.voucher.line create(vals)
                        cr_line = {
                            'voucher_id': rec.id,
                            'move_line_id': move_line_id,
                            'order_id': order.id
                        }
                        move_line_update = self.env['account.voucher.line'].sudo().with_context(
                            {'journal_id': rec.journal_id.id}).onchange_move_line_id(move_line_id)
                        cr_line.update(move_line_update.get('value', False))

                        # Check if voucher amount > unreconciled
                        # Set amount = amount unreconciled if difference allows otherwise set  "Attention required"
                        amount_unreconciled = cr_line.get('amount_unreconciled', 0)
                        if amount > amount_unreconciled:
                            if (amount - amount_unreconciled) <= VOUCHER_MAX_OVERPAY:
                                amount = amount_unreconciled
                            else:
                                auto_validation = 'a'
                                continue

                        # Amount
                        cr_line.update({'amount': amount})

                        # Mark voucher as fully reconciled if it really is
                        if amount == amount_unreconciled:
                            cr_line.update({'reconcile': True,})

                        # Create voucher line
                        self.env['account.voucher.line'].sudo().create(cr_line)


                    # Validate voucher or send a message that attention is required!
                    if auto_validation == 'v':
                        rec.proforma_voucher()
                    elif auto_validation == 'a':
                        # Post messages to Sales Orders and Vouchers
                        message_text = _("Incoming payment number ") + rec.reference + \
                                       _(" in amount of ") + str(amount) + _(" requires manual validation by accountant! "
                                                                                         "Payment reference: ") + rec.name
                        # Notify orders
                        for confirmed_order in confirmed_orders:
                            confirmed_order.sudo().message_post(type="notification", subtype="mt_comment", body=message_text)
                        # Notify voucher
                        rec.sudo().message_post(type="notification", subtype="mt_comment", body=message_text)
                    # Write auto validation status
                    rec.auto_validation = auto_validation

        # Mark Error for non validated orders
        if len(not_validated) > 0:
            self.browse(not_validated).write({'auto_validation': 'e'})


# -- Find Vouchers with several Sales Order references and mark them as "Attention required"
    """
    If multiple Sales Order detected in single voucher send notification to related Sales Orders and voucher,
     mark voucher as "Attention required" 
    """
    @api.multi
    def find_attention_required(self):
        # Array for not validated orders
        for rec in self:

            # Skip if payment information is missing
            if not rec.name or not rec.reference:
                continue

            # Proceed Sales Order only for the moment. Capitalize before detection
            order_type = 'sale.order'

            res = self.parse_document(order_type, rec.partner_id.id, rec.company_id.id, rec.name.upper())
            # _logger.info("Parser output: %s", res)
            if not res:
                continue
            else:
                unchecked_ids = res['confirmed_ids']
                if not unchecked_ids or len(unchecked_ids) < 1:
                    # Mark voucher auto validation error
                    continue
                else:
                    # Remove duplicated ids if any
                    confirmed_ids = [unchecked_ids[0]]
                    for unchecked_id in unchecked_ids:
                        if unchecked_id not in confirmed_ids:
                            confirmed_ids.append(unchecked_id)

                    # Proceed only if several Sales Orders in one voucher
                    if len(confirmed_ids) == 1:
                        continue
                    confirmed_orders = self.env['sale.order'].sudo().browse(confirmed_ids)
                    message_text = _("Incoming payment number ") + rec.reference + _(" in amount of ") + str(rec.amount) +_(
                        " requires manual validation by accountant! "
                        "Payment reference: ") + rec.name

                    # Notify Sales Orders
                    for order in confirmed_orders:
                        order.sudo().message_post(type="notification", subtype="mt_comment", body=message_text)

                    # Notify voucher
                    rec.sudo().message_post(type="notification", subtype="mt_comment", body=message_text)
                    # Write auto validation status
                    rec.auto_validation = 'a'


########################
# Account voucher line #
########################
class DMAccountVoucherLine(models.Model):
    _inherit = "account.voucher.line"

# -- Fields
    order_id = fields.Many2one(string="Sales Order", comodel_name='sale.order')

# -- Move line change
    @api.multi
    def onchange_move_line_id(self, line_id):
        if not line_id:
            return
        move_line = self.env['account.move.line'].browse([line_id])
        # _logger.info("Context %s", self._context)
        currency = move_line.currency_id or move_line.company_id.currency_id
        journal_id = self._context.get('journal_id', False)
        voucher_currency = False
        if journal_id:
            journal = self.env['account.journal'].browse([journal_id])
            if journal:
                voucher_currency = journal.currency if journal.currency else journal.company_id.currency_id
        if not voucher_currency:
            voucher_currency = currency

        res = {
            'currency_id': currency.id,
            'account_id': move_line.account_id.id,
            'type': 'dr' if move_line.credit else 'cr',
        }

        if currency == voucher_currency:
            res.update({
                'amount_original': move_line.credit or move_line.debit or 0.0,
                'amount_unreconciled': abs(move_line.amount_residual),
            })
        else:
            res.update({
                'amount_original': voucher_currency.compute(move_line.credit
                                                    or move_line.debit or 0.0, currency),
                'amount_unreconciled': voucher_currency.compute(abs(move_line.amount_residual), currency),
            })

        return {
            'value': res,
        }

# -- Create
    @api.model
    def create(self, vals):

        # Try to detect Sales Order if not passed in vals
        order_id = vals.get('order_id', False)
        if not order_id:
            # Get move line
            move_line = self.env['account.move.line'].sudo().browse([vals.get('move_line_id', False)])

            # 1. Get related invoice
            self._cr.execute(""" SELECT id FROM account_invoice inv
                                     WHERE inv.move_id = %s """,
                             (move_line.move_id.id,))

            inv_id = self._cr.fetchone()[0] or False
            if not inv_id:
                return super(DMAccountVoucherLine, self).create(vals)

            # 2. Get Sales Order by invoice
            self._cr.execute(""" SELECT order_id FROM sale_order_invoice_rel soi
                                    WHERE soi.invoice_id = %s """,
                             (inv_id,))
            order_ids = self._cr.fetchall()[0]
            vals['order_id'] = order_ids[0] if len(order_ids) == 1 else False

        # Create
        res = super(DMAccountVoucherLine, self).create(vals)

        if not order_id:
            return res

        # Send notification to Sales Order if any linked
        # Check context and send messages to related Sales Orders
        # Trigger "proceed payment" for Sales Orders
        amount = res.amount

        notify = self._context.get('notify_so', False)
        if notify:
            # Get Sales Orders ids from vals
            if notify == 'manual':
                message_text = _("Incoming payment number ") + res.voucher_id.reference + _(" in amount of ") + str(amount) + _(" was validated by ") + self.env.user.name + _(". Payment reference: ") + res.voucher_id.name
            elif notify == 'auto':
                message_text = _("Incoming payment number ") + res.voucher_id.reference + _(" in amount of ") + str(
                    amount) + _(" was validated automatically."
                                " Payment reference: ") + res.voucher_id.name
            # Send it!
            res.order_id.message_post(type="notification", subtype="mt_comment", body=message_text)

        return res

