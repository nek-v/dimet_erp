from openerp import models, fields, api, _, tools
from datetime import datetime, timedelta
import logging
# Logger for debug
_logger = logging.getLogger(__name__)

PAYMENT_TERMS = {'0': 'Advanced payment',
                 '1': 'Order confirmation',
                 '2': 'Ready for delivery',
                 '3': 'Delivered',
                 '4': 'Final acceptance',
                 '5': 'Fixed date',
                 '6': 'Other payment',
                 '9': 'Other'}


############################
# Planned payment template #
############################
class DMPaymentPlannedTemplate(models.Model):
    _name = "dm.payment.planned.template"

# -- Fields
    name = fields.Char(string="Description", compute='_name_compose', store=True)
    parent_id = fields.Many2one(string="Previous payment", comodel_name='dm.payment.planned.template')

    term_id = fields.Many2one(string="Related Term", comodel_name='account.payment.term', auto_join=True,
                              ondelete="cascade")

    # Amounts
    percent = fields.Float(string="%", track_visibility="onchange")

    payment_term = fields.Selection([
        ('0', 'Advanced payment'),
        ('1', 'Order confirmation'),
        ('2', 'Ready for delivery'),
        ('3', 'Delivered'),
        ('4', 'Final acceptance'),
        ('5', 'Fixed date'),
        ('6', 'Other payment'),
    ], string="Payment term", required=True, track_visibility='onchange',
        help="What conditions must be met to trigger the payment process.\
                  \n Some conditions are checked automatically, some must be checked manual.\
             \n Anyways re-check all payment conditions for further confidence! "
    )

    days_term = fields.Integer(string="Days", help="Days for payment to be done after payment condition is met",
                               track_visibility="onchange")

# -- Compose name
    @api.multi
    @api.depends('percent', 'payment_term')
    def _name_compose(self):
        for rec in self:
            rec.name = str(round(rec.percent, 2)) + '% ' + _(
                PAYMENT_TERMS.get(rec.payment_term, ''))


###################
# Planned payment #
###################
class DMPaymentPlanned(models.Model):
    _name = "dm.payment.planned"
    _description = "Planned payment"

    _inherit = ['mail.thread', 'ir.needaction_mixin']

# -- Fields
    sequence = fields.Integer(string="Priority")
    parent_id = fields.Many2one(string="Previous payment", comodel_name='dm.payment.planned')
    name = fields.Char(string="Description", compute='_name_compose', store=True)

    order_id = fields.Many2one(string="Related Order", comodel_name='sale.order', auto_join=True, ondelete="cascade")
    order_line_ids = fields.Many2many(string="Order Lines", comodel_name='sale.order.line',
                                      relation='dm_payment_planned_so_line_rel',
                                      column1='payment_id', column2='line_id',
                                      help="Leave blank if delivery of all items is required!")

    partner_id = fields.Many2one(string="Partner", related='order_id.partner_invoice_id', readonly=True, auto_join=True)
    company_id = fields.Many2one(related='order_id.company_id', readonly=True, auto_join=True)
    user_id = fields.Many2one(related='order_id.user_id', readonly=True, store=True)

    # Amounts
    percent = fields.Float(string="%", track_visibility="onchange")
    percent_computed = fields.Float(string="%", help="% computed based on payment amount", compute='_percent_computed')

    amount_order = fields.Float(string="Order amount", compute='_amount_order')
    amount_total = fields.Float(string="Payment amount", compute='compute_total', inverse='_set_total', store=True)
    amount_paid = fields.Float(string="Paid amount")
    amount_remaining = fields.Float(string="Remaining amount", compute="_compute_balance", store=True,
                                    track_visibility="onchange")
    amount_remaining_str = fields.Char(string="Remaining amount", compute="_amount_remaining_str")

    note = fields.Char(string="Note", help="Additional information")

    state = fields.Selection([
        ('0', 'Draft'),
        ('1', 'Confirmed'),
        ('s', 'Scheduled'),
        ('p', 'Paid'),
        ('c', 'Cancelled'),
    ], string="State", default='0', required=True, track_visibility='onchange')

    payment_term = fields.Selection([
        ('0', 'Advanced payment'),
        ('1', 'Order confirmation'),
        ('2', 'Ready for delivery'),
        ('3', 'Delivered'),
        ('4', 'Final acceptance'),
        ('5', 'Fixed date'),
        ('6', 'Other payment'),
    ], string="Payment term", required=True, track_visibility='onchange',
        help="What conditions must be met to trigger the payment process.\
                  \n Some conditions are checked automatically, some must be checked manual.\
             \n Anyways re-check all payment conditions for further confidence! "
    )

    days_term = fields.Integer(string="Days", help="Days for payment to be done after payment condition is met",
                               track_visibility="onchange")
    date_planned = fields.Date(string="Planned date", track_visibility="onchange",
                               help="Date when payment is planned to be done after payment condition is met.\
                                                           \n Set '0' if payment is done the same date or before condition is met.")
    days_planned = fields.Integer(string="Days planned", compute='_days_planned', store=False,
                                     help="Days to payment calculated bases on date planned")
    date_planned_calendar = fields.Date(string="Planned date", compute='_date_planned_calendar')

    date_triggered = fields.Date(string="Date triggered", readonly=True, help="Date when payment condition was actually met")

    date_calculated = fields.Date(string="Calculated date", help="Date calculated based on payment term")
    days_calculated = fields.Integer(string="Days calculated", compute='_days_calculated', store=False,
                                     help="Days to payment calculated bases on payment term")

    date_paid = fields.Date(string="Date paid", readonly=True)
    days_overdue = fields.Integer(string="Days overdue",
                                  help="Final overdue = difference between date calculated and date paid")
    overdue_state = fields.Selection([
        ('0', 'All fine :)'),
        ('1', 'Payment date soon!'),
        ('2', 'Overdue < 7 days'),
        ('3', 'Overdue < 14 days'),
        ('4', 'Overdue >= 14 days'),
    ], default=False, string="Overdue", compute="_overdue_state")

# -- Write (test only)
    @api.multi
    def write_temp(self, vals):
        _logger.info("Write context = %s", self._context)
        super(DMPaymentPlanned, self).write(vals)

# -- Compute overdue state
    @api.multi
    @api.depends('days_calculated', 'days_planned')
    def _overdue_state(self):
        for rec in self:
            days_calculated = rec.days_calculated if rec.days_calculated < rec.days_planned else rec.days_planned
            if days_calculated > 3:
                rec.overdue_state = '0'
            elif 3 >= days_calculated >= 0:
                rec.overdue_state = '1'
            elif 0 > days_calculated > -7:
                rec.overdue_state = '2'
            elif -7 >= days_calculated > -14:
                rec.overdue_state = '3'
            elif -14 >= days_calculated:
                rec.overdue_state = '4'
            else:
                rec.overdue_state = False

# -- Planned date for Calendar view
    @api.multi
    @api.depends('date_planned')
    def _date_planned_calendar(self):
        for rec in self:
            rec.date_planned_calendar = rec.date_planned

# -- Remaining amount as string
    @api.multi
    @api.depends('amount_remaining')
    def _amount_remaining_str(self):
        for rec in self:
            rec.amount_remaining_str = "{:16,.2F}".format(rec.amount_remaining)

# -- Trigger planned payment term
    """ Use 'triggers' :parameter to launch trigger.
    Triggers is a list of payment_term    
    """
    @api.multi
    def trigger_term(self, triggers=False):
        if not triggers:
            return
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT

        for rec in self:

            # Continue if already triggered
            if rec.state in ['s', 'p', 'c']:
                continue

            # If not our trigger - continue
            payment_term = rec.payment_term
            if payment_term not in triggers:
                continue

            vals = {'state': 's'}


            # Fixed date
            if payment_term == '5':
                vals.update({
                    'date_triggered': rec.order_id.date_order,
                    'date_calculated': rec.date_planned,
                })

            # Other payment base on date paid
            elif payment_term == '6':
                date_paid = rec.parent_id.date_paid
                if not date_paid:
                    continue
                date_calculated = datetime.strptime(date_paid, date_format) + timedelta(days=rec.days_term)
                vals.update({
                    'date_triggered': date_paid,
                    'date_calculated': date_calculated,
                    'date_planned': date_calculated,
                })

            # Order confirmation
            elif payment_term == '1':
                datetime_format = tools.DEFAULT_SERVER_DATETIME_FORMAT
                date_order = rec.order_id.date_order
                date_calculated = datetime.strptime(date_order, datetime_format) + timedelta(days=rec.days_term)
                vals.update({
                    'date_triggered': date_order,
                    'date_calculated': date_calculated,
                    'date_planned': date_calculated,
                })

            # Ready for delivery
            elif payment_term == '2':
                # Get Sales Order Line
                line_ids = rec.order_line_ids if rec.order_line_ids else rec.order_id.order_line
                not_all = date_max = False     # True if not all Sales Order Lines are ready
                for so_line in line_ids:
                    if so_line.date_ready is False and so_line.product_id.type != 'service':
                        not_all = True
                        break
                    else:
                        if so_line.date_ready > date_max:
                            date_max = so_line.date_ready

                # Exit if not all lines satisfy condition
                if not_all or not date_max:
                    continue

                date_calculated = datetime.strptime(date_max, date_format) + timedelta(days=rec.days_term)
                vals.update({
                    'date_triggered': date_max,
                    'date_calculated': date_calculated,
                    'date_planned': date_calculated,
                })

            # Delivered
            elif payment_term == '3':
                # Get Sales Order Line
                line_ids = rec.order_line_ids if rec.order_line_ids else rec.order_id.order_line
                not_all = date_max = False     # True if not all Sales Order Lines are ready
                for so_line in line_ids:
                    if so_line.date_delivered is False and so_line.product_id.type != 'service':
                        not_all = True
                        break
                    else:
                        if so_line.date_delivered > date_max:
                            date_max = so_line.date_delivered

                # Exit if not all lines satisfy condition
                if not_all or not date_max:
                    continue

                date_calculated = datetime.strptime(date_max, date_format) + timedelta(days=rec.days_term)
                vals.update({
                    'date_triggered': date_max,
                    'date_calculated': date_calculated,
                    'date_planned': date_calculated,
                })

            # Final acceptance
            elif payment_term == '4':
                # Get Sales Order Line
                line_ids = rec.order_line_ids if rec.order_line_ids else rec.order_id.order_line
                not_all = date_max = False  # True if not all Sales Order Lines are ready
                for so_line in line_ids:
                    if not so_line.date_accepted:
                        not_all = True
                        break
                    else:
                        if so_line.date_accepted > date_max:
                            date_max = so_line.date_accepted

                # Exit if not all lines satisfy condition
                if not_all or not date_max:
                    continue

                date_calculated = datetime.strptime(date_max, date_format) + timedelta(days=rec.days_term)
                vals.update({
                    'date_triggered': date_max,
                    'date_calculated': date_calculated,
                    'date_planned': date_calculated,
                })

            # Write result
            rec.write(vals)

# -- Get computed percent. Need this field to check constraints
    @api.multi
    @api.depends('amount_total')
    def _percent_computed(self):
        for rec in self:
            order_total = rec.order_id.amount_total
            if rec.order_id.amount_total > 0 and rec.amount_total > 0:
                rec.percent_computed = rec.amount_total / order_total * 100
            else:
                rec.percent_computed = rec.percent

# -- Get order or items total amount
    @api.multi
    @api.depends('payment_term')
    def _amount_order(self):
        for rec in self:
            if rec.payment_term in ['a', 'r', 'd', 'i'] and rec.order_line_ids:
                amount_items_total = 0
                for line in rec.order_line_ids:
                    amount_items_total += (line.tax_id.compute_all(line.price_unit, line.product_uom_qty)).get('total_included', 0)
                    rec.amount_order = amount_items_total
            else:
                rec.amount_order = rec.order_id.amount_total

# -- Set percent when amount is changed
    @api.multi
    # @api.onchange('amount_total', 'order_line_ids') # Onchange not needed for inverse computed fields!)
    def _set_total(self):
        for rec in self:
            if rec.amount_order > 0 and rec.amount_total > 0:
                rec.percent = rec.amount_total * 100 / rec.amount_order
               
# -- Compute amount_total
    @api.multi
    @api.depends('percent', 'order_line_ids', 'order_id.amount_total')
    def compute_total(self):
        for rec in self:
            rec.amount_total = rec.percent * rec.amount_order / 100

# -- Get days calculated
    @api.multi
    @api.depends('date_calculated')
    def _days_calculated(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        date_now = datetime.today()
        for rec in self:
            rec.days_calculated = (datetime.strptime(rec.date_calculated, date_format) - date_now).days + 1 if rec.date_calculated else False

# -- Get days planned
    @api.multi
    @api.depends('date_planned')
    def _days_planned(self):
        date_format = tools.DEFAULT_SERVER_DATE_FORMAT
        date_now = datetime.today()
        for rec in self:
            rec.days_planned = (datetime.strptime(rec.date_planned, date_format) - date_now).days + 1 if rec.date_planned else False

# -- Dummy
    @api.multi
    def _dummy(self):
        return

# -- Compute remaining amount
    @api.multi
    @api.depends('amount_total', 'amount_paid')
    def _compute_balance(self):
        for rec in self:
            rec.amount_remaining = rec.amount_total - rec.amount_paid


# -- Compose name
    @api.multi
    @api.depends('percent', 'order_id.name')
    def _name_compose(self):
        for rec in self:
            rec.name = rec.order_id.name + ' ' + str(round(rec.percent, 2)) + '% ' if rec.order_id else False


##########################
# Planned payment wizard #
##########################
class DMPaymentPlannedWizard(models.TransientModel):
    _name = "dm.payment.planned.wizard"
    _description = "Planned payment wizard"

    payment_term = fields.Selection([
        ('0', 'Advanced payment'),
        ('1', 'Order confirmation'),
        ('2', 'Ready for delivery'),
        ('3', 'Delivered'),
        ('4', 'Final acceptance'),
        ('5', 'Fixed date'),
        ('6', 'Other payment'),
    ], string="Payment term", required=True, track_visibility='onchange',
        help="What conditions must be met to trigger the payment process.\
                  \n Some conditions are checked automatically, some must be checked manual.\
             \n Anyways re-check all payment conditions for further confidence! "
    )
