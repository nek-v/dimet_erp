from openerp import models, fields, api


class DMHREmployee(models.Model):
    _inherit = "hr.employee"

    # Fields
    signature = fields.Html(string="Signature", related='user_id.signature', groups='base.group_hr_user')


class DMHRExpense(models.Model):
    _inherit = "hr.expense.expense"

    # Fields
    calendar_events = fields.Many2many(string="Related events", comodel_name='calendar.event',
                                       relation='dm_event_expense',
                                       column1='event_id',
                                       column2='expense_report_id')
