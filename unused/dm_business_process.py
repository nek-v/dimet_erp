from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


# -- Compose domain
def compose_domain(process, state):
    domain = "['&', ('process','='," + str(process) + "), ('state','=','" + state + "')]"
    return domain


#################
# Process stage #
#################
class DMProcessStage(models.Model):
    _name = 'dm.process.stage'
    _description = "Dimet Business Process Stage"

    # Fields
    sequence = fields.Integer(string="Priority", default=100)
    name = fields.Char(string="Name", translate=True)
    processes = fields.Many2many(string="Stages", comodel_name='dm.business.process',
                                 relation='dm_process_stage_rel',
                                 column1='stage',
                                 column2='process')
    rules = fields.One2many(string="Rules", comodel_name='dm.process.rule', inverse_name='stage')
    logs = fields.One2many(string="Stage logs", comodel_name='dm.process.stage.log',
                           inverse_name='stage')


####################
# Business Process #
####################
class DMBusinessProcess(models.Model):
    _name = 'dm.business.process'
    _description = "Dimet Business Process"

    # Fields
    name = fields.Char(string="Name", translate=True, required=True)
    description = fields.Text(string="Description", translate=True)
    rules = fields.One2many(string="Access filters", comodel_name='dm.process.rule',
                            inverse_name='process')
    stages = fields.Many2many(string="Stages", comodel_name='dm.process.stage',
                              relation='dm_process_stage_rel',
                              column1='process',
                              column2='stage')
    tasks = fields.One2many(string="Tasks", comodel_name='dm.process.task',
                            inverse_name='process')

    # SQL constraints
    _sql_constraints = [('dm_business_process_unique',
                         'UNIQUE (name)',
                         _('This business process already exists!'))]


################
# Process task #
################
class DMProcessTask(models.Model):
    _name = 'dm.process.task'
    _description = "Dimet Business Process Task"

    # Fields
    process = fields.Many2one(string="Business process", comodel_name='dm.business.process', required=True)
    parent_id = fields.Integer(string="Parent task")
    priority = fields.Selection([
        ('l', 'Low'),
        ('r', 'Regular'),
        ('h', 'High')
    ], string="Priority", required=True, default='1', translate=True)
    state_logs = fields.One2many(string="State logs", comodel_name='dm.process.stage.log', inverse_name='task')
    name = fields.Char(string="Name", required=True, translate=True)
    description = fields.Char(string="Description", required=True, translate=True)


#########################
# Business Process Rule #
#########################
class DMProcessRule(models.Model):
    _name = 'dm.process.rule'
    _description = "Dimet Business Process Rule"

    # Fields
    process = fields.Many2one(string="Process", comodel_name='dm.business.process')
    stage = fields.Many2one(string="Stage", comodel_name="dm.process.stage")
    groups = fields.Many2many(string="Groups", comodel_name='res.groups',
                              relation='dm_proc_rule_group_rel',
                              column1='proc_rule',
                              column2='group_id')
    perm_read = fields.Boolean(string="For Read")
    perm_write = fields.Boolean(string="For Write")
    perm_create = fields.Boolean(string="For Create")
    ir_rule = fields.Many2one(string="Odoo rule ID", comodel_name='ir.rule', ondelete='cascade')


##############
# res.groups #
##############
class DMResGroups2(models.Model):
    _name = 'res.groups'
    _inherit = 'res.groups'

    # Fields
    name = fields.Char(translate=True)
    dm_process_rules = fields.Many2many(string="Groups", comodel_name='dm.process.rule',
                                        relation='dm_proc_rule_group_rel',
                                        column1='group_id',
                                        column2='proc_rule')


#####################
# Process stage log #
#####################
class DMProcessStageLog(models.Model):
    _name = 'dm.process.stage.log'
    _description = "Dimet Business Process Stage Log"

    # Fields
    task = fields.Many2one(string="Task", comodel_name='dm.process.task')
    stage = fields.Many2one(string="Stage", comodel_name='dm.process.stage')

